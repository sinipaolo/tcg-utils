import sys
import json

import requests

if len(sys.argv) < 2:
    print("Command usage: python get_binder.py [POKEMON_NAME]")
    exit(1)

target_pokemon = sys.argv[1]

res = requests.get(
   f"https://pokemoncard.io/api/search.php?limit=100&n={target_pokemon}&desc={target_pokemon}&sort=name"
)

pokemons = res.json()

for pokemon in pokemons:

    pokemon["name"] = pokemon["name"].replace("δ", "δ Delta Species")
    pokemon["name"] = pokemon["name"].replace(" Star ", " Gold Star ")
    pokemon["name"] = pokemon["name"].replace(" G ", " [G] ")

    if pokemon["name"].endswith(" ex"):
        print(pokemon["name"])
        continue

    set_pokemons = requests.get(f"https://raw.githubusercontent.com/PokemonTCG/pokemon-tcg-data/master/cards/en/{pokemon['setCode']}.json").json()

    for set_pokemon in set_pokemons:
        if set_pokemon["id"] == pokemon["id"]:
            if (pokemon_level := set_pokemon.get("level")) and "LV." not in pokemon["name"]:
                pokemon["name"] += f" lv.{pokemon_level}"

    if pokemon["abilityName"]:
        if pokemon["attack3Name"]:
            print(f'{pokemon["name"]} [{pokemon["abilityName"]} | {pokemon["attack1Name"]} | {pokemon["attack2Name"]} | {pokemon["attack3Name"]}] ({pokemon["ptcgoCode"]})')
            continue
        if pokemon["attack2Name"]:
            print(f'{pokemon["name"]} [{pokemon["abilityName"]} | {pokemon["attack1Name"]} | {pokemon["attack2Name"]}] ({pokemon["ptcgoCode"]})')
            continue
        if pokemon["attack1Name"]:
            print(f'{pokemon["name"]} [{pokemon["abilityName"]} | {pokemon["attack1Name"]}] ({pokemon["ptcgoCode"]})')
            continue
        else:
            print(f'{pokemon["name"]} [{pokemon["abilityName"]}] ({pokemon["ptcgoCode"]})')
            continue

    if pokemon["attack3Name"]:
        print(f'{pokemon["name"]} [{pokemon["attack1Name"]} | {pokemon["attack2Name"]} | {pokemon["attack3Name"]}] ({pokemon["ptcgoCode"]})')
        continue
    if pokemon["attack2Name"]:
        print(f'{pokemon["name"]} [{pokemon["attack1Name"]} | {pokemon["attack2Name"]}] ({pokemon["ptcgoCode"]})')
        continue
    if pokemon["attack1Name"]:
        print(f'{pokemon["name"]} [{pokemon["attack1Name"]}] ({pokemon["ptcgoCode"]})')
        continue
    else:
        print(f'{pokemon["name"]} ({pokemon["ptcgoCode"]})')
        continue

